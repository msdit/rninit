import { createStore, applyMiddleware } from "redux";
import { createReactNavigationReduxMiddleware } from "react-navigation-redux-helpers";
import thunk from "redux-thunk";

import getRootReducer from "./reducers";
import RootNavigator from './configs/routes/index';

const navReducer = (state, action) => {
  const newState = RootNavigator.router.getStateForAction(action, state);
  return newState || state;
};

createReactNavigationReduxMiddleware(
  'root',
  state => state.nav
)

export default store = createStore(
  getRootReducer(navReducer),
  applyMiddleware(thunk)
);