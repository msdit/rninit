import React from 'react';
import { Button, Text, View } from 'react-native';

import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as actions from "../actions/home";

class Login extends React.Component {
  render() {
    return (
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
        <Text
          style={{
            fontFamily: 'Rubik',
            fontWeight: '900',
            fontSize:30
          }}
        >
          {`Login`}
        </Text>
      </View>
    );
  }
}

export default connect(
	state => ({
		...state
	}),
	dispatch => bindActionCreators(actions, dispatch)
)(Login);