import React from 'react';
import { Button, Text, View } from 'react-native';
import { StackActions, NavigationActions } from 'react-navigation';

import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as actions from "../actions/home";

class Splash extends React.Component {
  componentDidMount = () => {
    setTimeout(() => {
      this.navigateToHome();
    }, 500)
  }

  navigateToHome = () => {
    const resetAction = StackActions.reset({
      index: 0,
      actions: [NavigationActions.navigate({ routeName: this.props.navigation.getParam('routeTo') || 'MainApp' })],
    });
    this.props.navigation.dispatch(resetAction);
}

  render() {
    return (
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
        <Text
          style={{
            fontFamily: 'Rubik',
            fontWeight: '900',
            fontSize:30
          }}
        >
          {`Splash`}
        </Text>
      </View>
    );
  }
}

export default connect(
	state => ({
		...state
	}),
	dispatch => bindActionCreators(actions, dispatch)
)(Splash);