import React from 'react';
import { TouchableOpacity, StyleSheet, View, Text } from 'react-native';
import PropTypes from 'prop-types';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import Colors from '../../configs/colors'

function GridMenu (props) {
  const {list, onPress} = props;

  return(
    <View
      style={styles.grid}
    >
      {
        list.map((l, i) => (
          <TouchableOpacity
            style={[styles.col, { borderLeftColor: i===0 ? 'transparent' : Colors.rgba('primary', .1) }]}
            key={i}
            onPress={() => {onPress(l.route)}}
            activeOpacity={1}
          >
            <Icon
              name={l.icon}
              size={30}
              color={Colors.hex('primary')}
              style={{
                marginBottom: 10
              }}
            />
            <Text style={[styles.textStyle]}>
              {l.title}
            </Text>
          </TouchableOpacity>
        ))
      }
      
      
    </View>
  )
}

const styles = StyleSheet.create({
  grid:{
    flex: 3,
    flexDirection: 'row'
  },
  col: {
    flex: 1,
    aspectRatio: 1.5,
    alignItems: 'center',
    justifyContent: 'center',
    borderBottomWidth: 1,
    borderBottomColor: Colors.rgba('primary', .1),
    borderLeftWidth: 1,
  },
  textStyle:{
    fontFamily: 'Rubik',
    fontSize: 15,
    fontWeight: 'bold'
  }
})

GridMenu.prototypes = {
  list: PropTypes.array.isRequired,
  onPress: PropTypes.func.isRequired
}

export default GridMenu;