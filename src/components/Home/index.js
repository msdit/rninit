import React, { Component } from 'react';
import { View, Text } from 'react-native';
import { Overlay, ListItem } from 'react-native-elements';

import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as actions from "../../actions/home";

import CountriesMenu from './CountriesMenu';
import CountriesList from './CountriesList';
import Slideshow from './Slideshow';
import GridMenu from './GridMenu';

class Home extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
      headerTitle: navigation.getParam('CountriesMenu'),
    };
  }

  constructor(props){
    super(props);
    this.state = {
      countriesMenuVisible: false,
      selectedCountry: 'UAE',
      slideshowDataSource: [
        {
          title: 'Title 1',
          caption: 'Caption 1',
          url: 'http://lorempixel.com/output/nature-q-c-640-480-5.jpg',
        }, {
          title: 'Title 2',
          caption: 'Caption 2',
          url: 'http://lorempixel.com/output/nature-q-c-640-480-3.jpg',
        }, {
          title: 'Title 3',
          caption: 'Caption 3',
          url: 'http://lorempixel.com/output/nature-q-c-640-480-4.jpg',
        },
      ],
      countries: [
        {"full_name":"Iran, Islamic Republic Of","code":"IR","iso3":"IRN","currency":"IRR","capital":"Tehran","phone":"98"},
        {"full_name":"United Arab Emirates","code":"AE","iso3":"ARE","currency":"AED","capital":"Abu Dhabi","phone":"971"},
        {"full_name":"Turkey","code":"TR","iso3":"TUR","currency":"TRY","capital":"Ankara","phone":"90"},
        {"full_name":"Georgia","code":"GE","iso3":"GEO","currency":"GEL","capital":"Tbilisi","phone":"995"},
        {"full_name":"Armenia","code":"AM","iso3":"ARM","currency":"AMD","capital":"Yerevan","phone":"374"}
      ],
      gridMenu: [
        {
          title: 'Trips',
          icon: 'briefcase',
          route: ''
        },
        {
          title: 'Events',
          icon: 'microphone-variant',
          route: ''
        },
        {
          title: 'Restaurants',
          icon: 'glass-cocktail',
          route: ''
        },
      ]
    }
  }

  componentWillMount() {
    this.updateNavBar();
  }

  updateNavBar = (value) => {
    this.props.navigation.setParams({
      CountriesMenu: (
        <CountriesMenu
          selectedValue={value || this.state.selectedCountry}
          onPress={this.toggleCountryMenu}
        />
      )
    })
  }

  toggleCountryMenu = (value) => {
    this.setState((prevState) => {
      return {
        countriesMenuVisible: !prevState.countriesMenuVisible,
        selectedCountry: value || prevState.selectedCountry
      }
    })
    Boolean(value) && this.updateNavBar(value)
  }

  navigateTo = (route) => {
    route === 'back'
      ? this.props.navigation.goBack()
      : this.props.navigation.navigate(route)
  }

  render() {
    return (
      <View style={{flex:1}}>
        <Slideshow 
          dataSource={this.state.slideshowDataSource}
          onPositionChanged={position => this.setState({ position })}
          onPress={(image, index) => {console.log(image,index)}}
        />

        <GridMenu
          list={this.state.gridMenu}
          onPress={(route) => this.navigateTo.bind(null, route)}
        />

        <Overlay
          isVisible={this.state.countriesMenuVisible}
          onBackdropPress={() => (this.toggleCountryMenu())}
          windowBackgroundColor="rgba(0, 0, 0, .75)"
          width="auto"
          height="auto"
        >
          <CountriesList
            list={this.state.countries}
            onPress={(country) => (this.toggleCountryMenu(country))}
          />
        </Overlay>
      </View>
    )
  }
}


export default connect(
	state => ({
		...state
	}),
	dispatch => bindActionCreators(actions, dispatch)
)(Home);