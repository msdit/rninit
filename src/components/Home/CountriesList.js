import React from 'react';
import { Dimensions } from 'react-native';
import PropTypes from 'prop-types';
import { ListItem } from 'react-native-elements';

function CountriesList (props) {
  const {list, onPress} = props;

  return(
    list.map((l, i) => (
      <ListItem
        key={i}
        title={`${l.full_name} (${l.iso3})`}
        onPress={() => {onPress(l.iso3)}}
        style={{
          width: Dimensions.get('screen').width * .75
        }}
      />
    ))
  )
}

CountriesList.prototypes = {
  list: PropTypes.array.isRequired,
  onPress: PropTypes.func.isRequired
}

export default CountriesList;