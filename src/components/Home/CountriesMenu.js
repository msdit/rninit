import React from 'react';
import PropTypes from 'prop-types';
import { Button } from 'react-native-elements';
import Icon from 'react-native-vector-icons/Ionicons';

const CountriesMenu =  ({onPress, selectedValue}) => {
  return (
    <Button
      onPress={() => {onPress()}}
      titleStyle={{
        fontSize: 17,
        fontWeight: "700"
      }}
      buttonStyle={{
        backgroundColor: "rgba(0, 0,0, .3)",
        width: 120,
        height: 34,
        borderColor: "transparent",
        borderWidth: 0,
        borderRadius: 17
      }}
      icon={
        <Icon
          name='ios-arrow-down'
          size={17}
          color='white'
        />
      }
      iconRight
      title={selectedValue}
    />
  )
}

CountriesMenu.propTypes = {
  onPress: PropTypes.func.isRequired,
  selectedValue: PropTypes.string.isRequired
}

export default CountriesMenu;