import React from 'react';
import { Button, Text, View } from 'react-native';
import { StackActions, NavigationActions } from 'react-navigation';

import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as actions from "../actions/home";

class Profile extends React.Component {

  navigateToLogin = () => {
    // const resetAction = StackActions.reset({
    //   index: 0,
    //   actions: [NavigationActions.navigate({ routeName: 'Login' })],
    // });
    // this.props.navigation.dispatch(resetAction);
    this.props.navigation.navigate('Splash', { routeTo: 'Login' })
  }

  render() {
    console.log(this.props)
    return (
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
        <Text
          style={{
            fontFamily: 'Rubik',
            fontWeight: '900',
            fontSize:30
          }}
        >
          {`Profile`}
        </Text>
        <Button
          title="Logout"
          onPress={this.navigateToLogin}
        />
      </View>
    );
  }
}

export default connect(
	state => ({
		...state
	}),
	dispatch => bindActionCreators(actions, dispatch)
)(Profile);