import actionTypes from './types';
import api from '../data/api';

function setInitialData(data) {
  return {
    type: actionTypes.HOME.SET_INITIAL_DATA,
    data
  }
}

export function fetchInitialData() {
	return function(dispatch, getState) {
			return api.getInitialData()
				.then((response) => response.json())
				.then((json) => dispatch(setInitialData(json)))
				.catch((err) => console.log(err)); // TODO: Error Managment
	}
}
