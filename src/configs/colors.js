const colorList = {
  primary: '6A1B9A',

  white: 'FFFFFF',
}

rgbString = (colorName) => {
  let color = colorList[colorName].split(/(\w{2})/g).filter((item) => (item));
  color.forEach((item, index, array) => { array[index] = parseInt(item, 16) });
  return color.join(', ');
}

rgb = (colorName) => (`rgb(${rgbString(colorName)})`)

rgba = (colorName, opacity = 1) => (`rgba(${rgbString(colorName)}, ${opacity})`)

hex = (colorName) => (`#${colorList[colorName]}`)

export default {
  rgb,
  rgba,
  hex
}