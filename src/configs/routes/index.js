import React from 'react';
import { Animated, Easing } from 'react-native';
import { createStackNavigator } from 'react-navigation';

import Splash from '../../components/Splash';
import Login from '../../components/Login';
import TabRoutes from './tabRoutes';

const Routes = {
  Splash : { screen: Splash },
  Login : { screen: Login },
  MainApp : { screen: TabRoutes },
}

const Options = {
  initialRouteName: 'Splash',
  defaultNavigationOptions: {
    header: null
  },
  transitionConfig : () => ({
  	transitionSpec: {
  		duration: 0,
  		timing: Animated.timing,
  		easing: Easing.step0,
  	},
  }),
}

export default RootNavigator = createStackNavigator(Routes, Options);