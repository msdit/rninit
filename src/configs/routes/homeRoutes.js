import { createStackNavigator } from 'react-navigation';

import colors from '../colors';

import Home from '../../components/Home/index';

const Routes = {
  Home : { screen: Home },
  
}

const Options = {
  initialRouteName: 'Home',
  defaultNavigationOptions: {
    headerStyle: {
      backgroundColor: colors.hex('primary'),
    },
    headerTintColor: colors.hex('white'),
    headerTitleStyle: {
      fontWeight: 'bold',
    },
  },
}

export default HomeRoutes = createStackNavigator(Routes, Options);