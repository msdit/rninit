import { createStackNavigator } from 'react-navigation';

import colors from '../colors';

import Settings from '../../components/Settings';
import Profile from '../../components/Profile';

const Routes = {
  Settings : { screen: Settings },
  Profile : { screen: Profile },
  
}

const Options = {
  initialRouteName: 'Settings',
  defaultNavigationOptions: {
    headerStyle: {
      backgroundColor: colors.hex('primary'),
    },
    headerTintColor: colors.hex('white'),
    headerTitleStyle: {
      fontWeight: 'bold',
    },
  },
}

export default SettingsRoutes = createStackNavigator(Routes, Options);