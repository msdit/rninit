import React from 'react';
import { Text } from 'react-native';
import { createBottomTabNavigator } from 'react-navigation';
import Icon from 'react-native-vector-icons/Ionicons';

import Colors from '../colors';

import HomeRoutes from './homeRoutes';
import SettingsRoutes from './settingsRoutes';

const Routes = {
  HomeTab : { screen: HomeRoutes },
  SettingsTab: { screen: SettingsRoutes },
}

const Options = {
  initialRouteName: 'HomeTab',
  defaultNavigationOptions: ({ navigation }) => ({
    tabBarIcon: ({ focused, horizontal, tintColor }) => {
      const { routeName } = navigation.state;

      const iconName = {
        HomeTab: `ios-information-circle${focused ? '' : '-outline'}`,
        SettingsTab: `ios-options`
      }[routeName];

      return <Icon name={iconName} size={25} color={tintColor} />
    },
    tabBarLabel: ({ tintColor, focused }) => {
      const { routeName } = navigation.state;

      const tabName = {
        HomeTab: `Home`,
        SettingsTab: `Settings`
      }[routeName];
      
      return <Text style={{ fontFamily: 'Rubik', fontWeight: focused ? '500' : '100', fontSize: 12, color:tintColor }} >{tabName}</Text>
    },
  }),
  tabBarOptions: {
    activeTintColor: Colors.hex('primary'),
    inactiveTintColor: 'gray',
  },
}

export default TabRoutes = createBottomTabNavigator(Routes, Options);