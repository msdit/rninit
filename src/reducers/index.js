import { combineReducers } from "redux";
import mainReducer from './mainReducer'

export default getRootReducer = (nav) => combineReducers({ nav, mainReducer })