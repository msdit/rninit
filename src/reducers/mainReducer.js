import actionTypes from '../actions/types';

const initialState = {};

export default function mainReducer(state = initialState, action) {
  switch (action.type) {
    case actionTypes.HOME.SET_INITIAL_DATA:
      return {
        ...state,
        initialData: action.data
      }

    default:
      return { ...state }
  }
}