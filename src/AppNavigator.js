import { connect } from 'react-redux';
import { reduxifyNavigator } from 'react-navigation-redux-helpers';

import RootNavigator from './configs/routes/index'

const AppWithNavigationState = reduxifyNavigator(
  RootNavigator,
  'root'
)

const mapStateToProps = state => ({
  state: state.nav
})

export default AppNavigator = connect(mapStateToProps)(AppWithNavigationState);