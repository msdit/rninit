'use strict';

// import { baseUrl } from '../../package.json';
import VersionNumber from 'react-native-version-number';

const versionName = VersionNumber.appVersion;
const versionCode = VersionNumber.buildVersion;
const applicationId = VersionNumber.bundleIdentifier;

const api  = {
  getInitialData () {
    return fetch(`./fakeData/initialData.json`, {
      method: 'GET',
      headers: {
        versionCode,
        versionName
      }
    });
  }
}

export default api